# Data Science Project | Public Attention and Asset Price Development
**Remark:** This project was done in the context of the lecture "Data Science in Techno-Socio-Economic Systems" at ETH Zürich. Many methods were introduced in the lecture, which therefore was our primary resource. Other resources are listed further down. Our results are summarised in a presentation, which is also on this repository.

## Project Description
Starting from the quote, “There’s no such thing as bad publicity”, by P.T. Barnum, we want to explore the relationship between public (media/online) attention for a given asset and its price development. To translate the quote into a testable hypothesis we ask ourselves the following questions:

- Are asset prices influenced by attention?
- Does sentiment matter?
- Does asset type matter?
- Can we create models to predict the change in stock price based on attention?

As hinted at by the questions we want to explore different asset types like value stocks, growth stocks and cryptocurrencies. To this end we picked a few representative assets for each type, however our analyses can be easily extended to any asset.

- **Value stocks:** JPMorgan Chase, Nestlé, Allianz, Berkshire Hathaway
- **Growth stocks:** Microsoft, Nvidia, Tesla, Wix.com
- **Cryptocurrencies:** Bitcoin, Dogecoin, Ethereum

To answer the questions we use a variety of statistical measures for correlation such as: Pearson correlation, mutual information, granger causality and transfer entropy. Our time series data ranges from the beginning of 2017 to the beginning of 2024, and has daily data. An example is shown in the figure below.
![Tesla Data](img/presentation_plots/TSLA_raw_data.png)

## Main tasks
### Data | How do we measure attention?
We need proxies to capture public attention, which is a latent variable. We have chosen proxies that are well established in literature, namely:
- **Google Trends Score:** From Google Trends (https://trends.google.de/trends?geo=CH&hl=de) we get daily data of the relative search volume (compared to the maximum search volume in a given time period) of a keyword / object. To acquire the data we use a python package (https://github.com/qztseng/google-trends-daily/tree/master) which overlaps 9 month long daily data (accessed using the pytrends API https://github.com/GeneralMills/pytrends) to keep the normalization constant. 
- **The GDELT Project:** The GDELT Project offers data on the daily percentage of articles mentioning a keyword (such as "Tesla") and the average sentiment of these articles. (https://www.gdeltproject.org)
- **Trading Volume:** We use Yahoo finance data on the daily trading volume of a stock. (https://finance.yahoo.com)

We have to make sure that these proxies for attention match and actually reflect the same latent variable. To this end we check pearson correlation and mutual information between them.

### Analysis | How do we check for correlation?
We use numerous correlation analyses to hopefully account for non-linear correlation, etc.
- Pearson correlation
- Mutual information
- Granger causality
- Transfer Entropy

### Conclusion | What have we learnt?
Working with financial data is quite challenging, so we had to use various tools to check if there is some correlation between public attention and stock price. We come to the conclusion that attention does have an impact on stock price - especially for cryptocurrencies. However, sentiment is not that relevant.

## Workflow
In the following section we briefly outline our approach, workflow and how we managed working in a team of five people.
1. get data.
2. make first analyses, estimate what is possible and interesting.
3. refine data acquisition process.
4. continue our analyses
5. try to create a var model, fail :(
6. interpret our results.


### Focus points
Franz
- [x] Data acquisition, google trends data
- [x] Transfer Entropy

Raphael
- [x] Transfer Entropy
- [x] Conclusion

Jonathan
- [x] Evaluation of the proxies
- [x] Mutual Information

Jakob
- [x] Data acquisition
- [x] Basic correlation

Simon
- [x] Data acquisition, GDELT data
- [x] Granger Causality

### Archive
- [ ] significance analysis
- [ ] synthesize results from our analyses
- [ ] structure presentation 
- [ ] get datasets for initial investigation: yahoo finance, gdelt project data, google trends data
- [ ] analyse if our time series are stationary or not / detrend if necessary
- [ ] granger causation analysis for all variables and titles
- [ ] significance analysis
- [ ] Implement vector autoregression model
- [ ] design presentation
- [ ] compare different proxies for attention: gdelt article volume, google trends, trading volume
- [ ] include sentiment score: is there a correlation between attention and sentiment?
- [ ] mutual information
- [ ] analyze the resulting heatmaps
- [ ] first approach for getting daily google trends data (rem: can be problematic in some cases)
- [ ] implement transfer entropy for our dataset (+significance analysis!!)
- [ ] Outline presentation
- [ ] implement transfer entropy for our dataset (+significance analysis!!)
- [ ] project outline, goals 
- [ ] get daily google trends data with overlap method
- [ ] improve, update create_data.ipynb
- [ ] update all datasets
- [ ] basic correlation analysis between attention metrics and asset price development
- [ ] include sentiment score in correlation analysis
- [ ] delay in correlation plots
- [ ] stationary data for correlation
- [ ] gitlab
- [ ] significance analysis 
