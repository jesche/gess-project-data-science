from pytrends.request import TrendReq
import pandas as pd

# Create pytrends object
pytrends = TrendReq(hl='en-US', tz=360)

# Set up your query
kw_list = ["wix.com"]

pytrends.build_payload(kw_list, timeframe='2014-04-01 2024-03-31', geo='')

# Get the interest over time
interest_over_time_df = pytrends.interest_over_time()

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)

# Print the data
print(interest_over_time_df)
