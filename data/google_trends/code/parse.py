from collections import defaultdict

# Function to parse the data from a file
def parse_file(filename):
    data = defaultdict(list)
    with open(filename, 'r') as file:
        current_month = None
        for line in file:
            line = line.strip()
            if line.startswith('date'):
                current_month = None
                continue
            if current_month is None:
                current_month = line.split()[0][:7]  # Extract month from date
            date, value = line.split()
            data[current_month].append(int(value))
    return data

# Parse data from both files
data1 = parse_file('wix.com/output.txt')
data2 = parse_file('wix.com/output_10years.txt')

# Merge data and calculate sums
merged_data = {}
counter = -1
monthh = ""
highest = 0
for month, values1 in data1.items():
    if(monthh != month):
        monthh = month
        counter = counter +1
    values2 = data2['2014-04'][counter]
    #print("month: " +month)
    #print(values1)
    #print("values2: " + str(values2))
    merged_values = [values2*len(values1)/sum(values1)*x for x in values1]
    for x in merged_values:
        if x > highest:
            highest = x
    merged_data[month] = merged_values

print(highest)
# Write merged data to a new file
with open('merged_output.txt', 'w') as file:
    for month, values in merged_data.items():
        counter = 1
        for value in values:
            res = round(100/highest*value)
            file.write(f"{month}-{counter:02} {res} \n")
            counter = counter +1
